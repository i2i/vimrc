" Note to self: learn this plugin that is installed:
" https://vimawesome.com/plugin/vim-multiple-cursors

execute pathogen#infect()
syntax on
filetype plugin indent on

" Remap exit insert mode to jk.
" Not recommended if you are always just kidding.
inoremap jk <ESC>

" Remap Ctrl to CapsLock (needs to be done at system level?)j
" inoremap <CapsLock> <CTRL>

" Basic settings

set modelines=0
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set encoding=utf-8
set scrolloff=3
set autoindent
set guifont=Menlo:h14
set wildmenu
set wildmode=list:longest
set number
set visualbell
set ttyfast
set ruler
set laststatus=2
set ignorecase
set smartcase
set showmatch
set hlsearch
set wrap
set linebreak
set nolist
set formatoptions=qrn1
" set colorcolumn=80

" Other settings
let mapleader = ","


" Control shortcuts

nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" Enable vim-javascript (https://github.com/pangloss/vim-javascript)
" set g:javascript_plugin_jsdoc = 1

" Only use JSX highlighting and indenting on .jsx files
" set g:jsx_ext_required = 1
